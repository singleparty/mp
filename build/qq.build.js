const path = require('path')
const moment = require('moment')
const execa = require('execa')
const mainVersion = '1.1'
const appToken = 'f11ac757f5a3720c048fa20209baac7d'

main()

async function main() {
  const time = moment().utcOffset(8).format('MMDDHH')
  const { stdout } = await execa('docker', [
    'run',
    '-e',
    `PLUGIN_VERSION=${mainVersion}.${time}`,
    '-e',
    `PLUGIN_DESC=版本描述`,
    '-e',
    `PLUGIN_APPTOKEN=${appToken}`,
    '-e',
    `PLUGIN_EXPERIENCE=true`,
    '-e',
    `PLUGIN_PREVIEW=false`,
    '-e',
    `PLUGIN_BUILDUSER=user`,
    '-e',
    `PLUGIN_FIRSTPAGE=pages/index/index`,
    '-e',
    `PLUGIN_USEPACKAGEJSON=false`,
    '-e',
    `PLUGIN_NPMBUILD=false`,
    '-v',
    `${path.join(__dirname, '../dist/dev/mp-qq')}:/tmp`,
    '-w',
    '/tmp',
    'qqminiapp/build:latest'
  ])

  console.log('uploadResult', stdout)
}
