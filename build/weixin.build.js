const mpci = require('miniprogram-ci')
const path = require('path')
const moment = require('moment')
const mainVersion = '1.1'

main()

async function main() {
  const time = moment().utcOffset(8).format('MMDDHH')
  const project = new mpci.Project({
    appid: 'wxeab35cd999e5e47d',
    type: 'miniProgram',
    projectPath: path.join(__dirname, '../dist/dev/mp-weixin'),
    privateKeyPath: path.join(__dirname, './private.wxeab35cd999e5e47d.key'),
    ignores: ['node_modules/**/*']
  })

  const uploadResult = await mpci.upload({
    project,
    version: `${mainVersion}.${time}`,
    desc: 'hello',
    setting: {
      es6: true,
      es7: true
    },
    robot: 1
    // onProgressUpdate: console.log
  })

  console.log('uploadResult', uploadResult)
}
