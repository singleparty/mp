import { defineConfig } from 'vite'
import path from 'path'
import uni from '@dcloudio/vite-plugin-uni'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  return {
    plugins: [uni()],
    build: {
      target: ['es2015'],
      rollupOptions: {}
    },
    resolve: {
      alias: {
        '@': path.join(__dirname, './src')
      }
    },
    base: mode === 'development' ? '/' : '/redpacket/',
    server: {
      proxy: {
        '^/(auth|user|redpacket)': {
          target: 'https://webapp.wallet.cashyuan.com:2514',
          secure: true,
          changeOrigin: true,
          configure: (proxy, options) => {
            proxy.on('proxyReq', function (proxyReq, req, res, options) {
              proxyReq.removeHeader('origin')
              proxyReq.removeHeader('referer')
            })
          }
        }
      }
    }
  }
})
